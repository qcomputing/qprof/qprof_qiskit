# ======================================================================================
#
# Copyright: CERFACS, LIRMM, Total S.A. - the quantum computing team (March 2021)
# Contributor: Adrien Suau (adrien.suau@cerfacs.fr)
#
# This program is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your discretion) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.
#
# See the GNU Lesser General Public License for more details. You should have received
# a copy of the GNU Lesser General Public License along with this program. If not, see
# https://www.gnu.org/licenses/lgpl-3.0.txt
#
# ======================================================================================

from typing import Union, Iterable
import re

from qiskit.circuit import QuantumCircuit, Instruction, Gate

from qprof.plugins.frameworks import interfaces


class RoutineWrapper(interfaces.RoutineWrapper):

    _circuit_name_update_regex = re.compile(r"^(.*)-\d+?$")

    def __init__(self, routine: Union[QuantumCircuit, Instruction, Gate]):
        super().__init__(routine)
        self._main_instruction: Instruction = routine
        if isinstance(self._main_instruction, QuantumCircuit):
            self._main_instruction = self._main_instruction.to_instruction()
        self._main_quantum_circuit: QuantumCircuit = self._main_instruction.definition

    def __iter__(self) -> Iterable["RoutineWrapper"]:
        for instr, qubits, clbits in self._main_quantum_circuit.data:
            yield RoutineWrapper(instr)

    @property
    def is_base(self):
        return self._main_quantum_circuit is None

    @staticmethod
    def _name_unupdate(name: str) -> str:
        while re.match(RoutineWrapper._circuit_name_update_regex, name):
            name = re.sub(
                RoutineWrapper._circuit_name_update_regex, r"\1", name, count=1
            )
        return name

    @property
    def name(self):
        return RoutineWrapper._name_unupdate(self._main_instruction.name)

    def __hash__(self):
        """Get the hash of the wrapped instruction

        :return: hash of the wrapped instruction
        """
        instr = self._main_instruction
        return hash((instr.name, tuple(instr.params)))

    def __eq__(self, other: "RoutineWrapper"):
        """Equality testing.

        :param other: right-hand side of the equality operator
        :return: True if self and other are equal, else False
        """
        sinstr: Instruction = self._main_instruction
        oinstr: Instruction = other._main_instruction
        return (
            sinstr.name == oinstr.name
            and len(sinstr.params) == len(oinstr.params)
            and all(sp == op for sp, op in zip(sinstr.params, oinstr.params))
        )
