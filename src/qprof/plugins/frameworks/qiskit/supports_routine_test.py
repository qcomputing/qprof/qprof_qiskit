# ======================================================================================
#
# Copyright: CERFACS, LIRMM, Total S.A. - the quantum computing team (March 2021)
# Contributor: Adrien Suau (adrien.suau@cerfacs.fr)
#
# This program is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your discretion) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.
#
# See the GNU Lesser General Public License for more details. You should have received
# a copy of the GNU Lesser General Public License along with this program. If not, see
# https://www.gnu.org/licenses/lgpl-3.0.txt
#
# ======================================================================================

import sys


def supports_routine(routine) -> bool:
    # If qiskit has not been imported, it cannot be a routine supported by this plugin.
    # This simple check avoids importing qiskit when it is not needed.
    if "qiskit" not in sys.modules:
        return False

    from qiskit.circuit import QuantumCircuit
    from qiskit.circuit.instruction import Instruction

    return isinstance(routine, Instruction) or isinstance(routine, QuantumCircuit)
